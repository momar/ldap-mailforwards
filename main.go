package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"github.com/go-ldap/ldap/v3"
	"github.com/leaanthony/clir"
)

type Mapping string
func (m Mapping) Check() bool {
	return strings.IndexByte(string(m), '=') >= 0 && m.Address() != "" && m.Filter() != ""
}
func (m Mapping) Address() string {
	return strings.TrimSpace(string(m[:strings.IndexByte(string(m), '=')]))
}
func (m Mapping) Filter() string {
	return strings.TrimSpace(string(m[strings.IndexByte(string(m), '=') + 1:]))
}

func main() {
	cli := clir.NewCli("ldap-mailforwards", "Synchronize LDAP attributes for mail forwards", "v0.0.1")

	ldapServer := "ldap://localhost:389"
	cli.StringFlag("server", "LDAP Server URL", &ldapServer)

	baseDN := ""
	cli.StringFlag("base-dn", "Base DN to use for searching", &baseDN)

	username := ""
	cli.StringFlag("username", "Bind username", &username)

	password := os.Getenv("BIND_PASSWORD")
	cli.StringFlag("password", "Bind password", &password)

	aliasAttribute := "mailGroupMember"
	cli.StringFlag("alias-attribute", "Alias attribute, defaults to mailGroupMember", &aliasAttribute)

	mappingsFile := ""
	cli.StringFlag("mappings-file", "File path to the alias configuration file, using the following format: list@example.org = (memberOf=cn=admin,*)", &mappingsFile)

	cli.Action(func() error {
		for {
			conn, err := ldap.DialURL(ldapServer)
			if err != nil {
				return fmt.Errorf("couldn't connect to LDAP server: %w", err)
			}
			err = conn.Bind(username, password)
			if err != nil {
				return fmt.Errorf("couldn't bind to LDAP server: %w", err)
			}
			
			mappingsFile, err := ioutil.ReadFile(mappingsFile)
			mappingsStrings := (strings.Split(string(mappingsFile), "\n"))
			mappings := []Mapping{}
			for _, mappingString := range mappingsStrings {
				if strings.TrimSpace(mappingString) == "" || strings.TrimSpace(mappingString)[0] == '#' {
					continue
				}
				mapping := Mapping(mappingString)
				if !mapping.Check() {
					return fmt.Errorf("invalid mapping: %s", mappingString)
				}
				mappings = append(mappings, mapping)
			}
			
			for _, mapping := range mappings {
				// Make sure everyone who should have an alias has it
				res, err := conn.Search(&ldap.SearchRequest{
					BaseDN: baseDN,
					Scope: ldap.ScopeWholeSubtree,
					Filter: "(&" + mapping.Filter() + "(!(" + aliasAttribute + "=" + mapping.Address() + ")))",
					Attributes: []string{},
				})
				if err != nil && !strings.Contains(err.Error(), "\"" + ldap.LDAPResultCodeMap[ldap.LDAPResultNoSuchObject] + "\"") {
					return fmt.Errorf("couldn't search for missing aliases for address '%s': %w", mapping.Address(), err)
				}
				for _, object := range res.Entries {
					err := conn.Modify(&ldap.ModifyRequest{
						DN: object.DN,
						Changes: []ldap.Change{{
							Operation: 0, // add
							Modification: ldap.PartialAttribute{
								Type: "1.3.6.1.4.1.1466.115.121.1.26",
								Vals: []string{
									mapping.Address(),
								},
							},
						}},
					})
					if err != nil {
						return fmt.Errorf("couldn't add missing alias '%s' for user '%s': %w", mapping.Address(), object.DN, err)
					} else {
						log.Printf("Added alias '%s' for user '%s'\n", mapping.Address(), object.DN)
					}
				}

				// Make sure everyone who shouldn't have an alias doesn't have it
				res, err = conn.Search(&ldap.SearchRequest{
					BaseDN: baseDN,
					Scope: ldap.ScopeWholeSubtree,
					Filter: "(&(!" + mapping.Filter() + ")(" + aliasAttribute + "=" + mapping.Address() + "))",
					Attributes: []string{},
				})
				if err != nil && !strings.Contains(err.Error(), "\"" + ldap.LDAPResultCodeMap[ldap.LDAPResultNoSuchObject] + "\"") {
					return fmt.Errorf("couldn't search for wrong aliases for address '%s': %w", mapping.Address(), err)
				}
				for _, object := range res.Entries {
					err := conn.Modify(&ldap.ModifyRequest{
						DN: object.DN,
						Changes: []ldap.Change{{
							Operation: 1, // delete
							Modification: ldap.PartialAttribute{
								Type: "1.3.6.1.4.1.1466.115.121.1.26",
								Vals: []string{
									mapping.Address(),
								},
							},
						}},
					})
					if err != nil {
						return fmt.Errorf("couldn't remove wrong alias '%s' for user '%s': %w", mapping.Address(), object.DN, err)
					} else {
						log.Printf("Removed alias '%s' for user '%s'\n", mapping.Address(), object.DN)
					}
				}
			}
			
			time.Sleep(1 * time.Minute)
		}
		return nil
	})
	err := cli.Run()
	if err != nil {
		log.Fatal(err)
	}
}
