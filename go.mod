module codeberg.org/momar/ldap-mailforwards

go 1.16

require (
	github.com/go-ldap/ldap/v3 v3.3.0 // indirect
	github.com/leaanthony/clir v1.0.4 // indirect
)
